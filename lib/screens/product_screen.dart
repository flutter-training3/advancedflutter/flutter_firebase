// ignore_for_file: use_build_context_synchronously

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ProductScreen extends StatefulWidget {
  const ProductScreen({Key? key}) : super(key: key);

  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  // อ่านข้อมูลจาก firestore
  final CollectionReference _product =
      FirebaseFirestore.instance.collection('products');

  // สร้างตัวแปรสำหรับเก็บ text field
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final _auth = FirebaseAuth.instance.currentUser;

  // สร้างฟังก์ชั่นเพิ่มและแก้ไขสินค้า
  Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    // ถ้ามีการส่งข้อมูลเก่ามา
    if (documentSnapshot != null) {
      action = 'update';
      _nameController.text = documentSnapshot['name'];
      _priceController.text = documentSnapshot['price'].toString();
    }
    // สร้าง popup แบบ bottomsheet
    await showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.only(
              top: 20,
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom + 20),
          child: SizedBox(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: _nameController,
                  decoration: const InputDecoration(
                    labelText: 'Product name : ',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: _priceController,
                  keyboardType: const TextInputType.numberWithOptions(
                    decimal: true,
                  ),
                  decoration: const InputDecoration(
                    labelText: 'Product price : ',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        onPressed: () async {
                          final String? name = _nameController.text;
                          final double? price =
                              double.tryParse(_priceController.text);
                          if (name != null && price != null) {
                            if (action == 'create') {
                              _product.add({
                                "name": name,
                                "price": price,
                                "createdDate": DateTime.now(),
                                "createdUser": _auth!.email
                              });
                            } else if (action == 'update') {
                              await _product.doc(documentSnapshot!.id).update({
                                "name": name,
                                "price": price,
                                "updatedDate": DateTime.now(),
                                "updatedUser": _auth!.email
                              });
                            }
                          }
                          // เครียร์ค่าว่าง
                          _nameController.text = '';
                          _priceController.text = '';
                          Navigator.pop(context);
                        },
                        child: Text(action == 'create'
                            ? 'Create Product'
                            : 'Update Product')),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  // สร้างฟังก์ชั่นการลบสินค้า
  Future<void> _deleteProduct(String productId) async {
    await _product.doc(productId).delete();
    // แสดง popup แจ้งผลการลบ
    ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(content: Text('Delete successfully.')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products'),
      ),
      body: StreamBuilder(
          stream: _product.orderBy('name').snapshots(),
          builder: (context, streamSnapshot) {
            if (streamSnapshot.hasData) {
              return ListView.builder(
                itemCount: streamSnapshot.data!.docs.length,
                itemBuilder: (context, index) {
                  final DocumentSnapshot documentSnapshot =
                      streamSnapshot.data!.docs[index];
                  return Card(
                    margin: const EdgeInsets.all(10),
                    child: ListTile(
                      title: Text(
                        documentSnapshot['name'],
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        '฿${documentSnapshot['price'].toString()}',
                        style: const TextStyle(fontSize: 16),
                      ),
                      trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            IconButton(
                                onPressed: () {
                                  _createOrUpdate(documentSnapshot);
                                },
                                icon: const Icon(
                                  Icons.edit,
                                  color: Colors.purple,
                                )),
                            IconButton(
                                onPressed: () {
                                  _deleteProduct(documentSnapshot.id);
                                },
                                icon: const Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                ))
                          ]),
                    ),
                  );
                },
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: _createOrUpdate,
        child: const Icon(Icons.add),
      ),
    );
  }
}
