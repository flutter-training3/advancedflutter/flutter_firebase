import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthenService {
  Future<String?> registration(
      {required String email, required password}) async {
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      return 'Success';
    } on FirebaseAuthException catch (firebaseError) {
      if (firebaseError.code == 'weak-password') {
        return 'The password is too weak';
      } else if (firebaseError.code == 'email-already-in-use') {
        return 'The account already exists for that email';
      } else {
        return firebaseError.message.toString();
      }
    } catch (err) {
      return err.toString();
    }
  }

  Future<String?> login({required String email, required password}) async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      return 'Success';
    } on FirebaseAuthException catch (firebaseError) {
      if (firebaseError.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (firebaseError.code == 'wrong-password') {
        return 'Wrong password provided for that user.';
      } else {
        return firebaseError.message.toString();
      }
    } catch (err) {
      return err.toString();
    }
  }
}
